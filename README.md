# ABOUT

Web version of Taboo game. Taboo is a word, guessing, and party game. The objective of the game is for a player to have their partners guess the word on the player's card without using the word itself or several additional words listed on the card. 

# SERVICES

1. __Web client__ - folder `client`
2. __Session api__ - folder `session` (files shared with __Websocket__ service)
3. __Websocket__ - folder `session` (files shared with __Session api__ service)
4. __Phrases api__ - folder `api-flask`

# HOW TO RUN

Each service has its own readme, which desribes setup for each one of them. Although suggested way of running the application is with the use of `Kubernetes` which is described in `k8s/README.md`