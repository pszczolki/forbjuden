import { useStores } from "@/store/provider";
import { Button, Popconfirm, Space, Typography } from "antd";
import { observer } from "mobx-react-lite";
import React, { useState } from "react";

const { Text } = Typography;

const RoundReadyPage = () => {
  const {
    isUserReady,
    setReadyForNextRoundAsync: setReadyForNextRound,
    abortGameAsync,
    users
  } = useStores();
  const [disable, setDisable] = useState(false);

  const setReady = () => {
    setDisable(true);
    setReadyForNextRound().finally(() => setDisable(false));
  };

  return (
    <div>
      <Text>Waiting for other players...</Text>
      <ul>
        {users.map(u => (
          <li key={u.userId}>
            {u.userId} : {u.readyToPlay ? "✔" : "❌"}
          </li>
        ))}
      </ul>
      <Space>
        <Button
          style={{ width: "100%" }}
          type="primary"
          onClick={setReady}
          disabled={disable || isUserReady}
        >
          Set ready
        </Button>
        <Popconfirm
          placement="top"
          title={"Are you sure you want to abort game?"}
          onConfirm={abortGameAsync}
          okText="Yes"
          cancelText="No"
        >
          <Button type="primary" danger>
            Leave game
          </Button>
        </Popconfirm>
      </Space>
    </div>
  );
};

export default observer(RoundReadyPage);
