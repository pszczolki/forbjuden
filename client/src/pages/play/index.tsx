import { useStores } from "@/store/provider";
import { observer } from "mobx-react-lite";
import React from "react";
import GuessingPage from "./guessing";
import ShowingPage from "./showing";
import WatchingPage from "./watching";

const PlayPage = () => {
  const { round, isYourTeamGuessing, areYouShowing, taboo } = useStores();

  const guessingWord = taboo?.guessingWord || "";
  const tabooWords = taboo?.tabooWords || [];

  const renderShowing = () => (
    <ShowingPage word={guessingWord} forbiddenWords={tabooWords} />
  );
  const renderGuessing = () => <GuessingPage />;
  const renderWatching = () => (
    <WatchingPage forbiddenWords={tabooWords} word={guessingWord} />
  );

  const render = () => {
    if (areYouShowing) {
      return renderShowing();
    } else if (isYourTeamGuessing) {
      return renderGuessing();
    } else {
      return renderWatching();
    }
  };

  return render();
};

export default observer(PlayPage);
