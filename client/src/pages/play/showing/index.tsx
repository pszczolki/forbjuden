import MainView from "@/components/mainView";
import TabooWord from "@/components/tabooWord";
import { useStores } from "@/store/provider";
import { Typography, Space, Button } from "antd";
import React from "react";
import "./style.scss";

const { Text, Title } = Typography;

type Props = TabooWordDto;

const ShowingPage = ({ word, forbiddenWords }: Props) => {
  const { scoreRoundAsync } = useStores();
  return (
    <div className="play-page">
      <MainView>
        <div>
          <Title type="secondary" level={5}>
            Try to prompt your teammates to guess this word:
          </Title>
          <TabooWord word={word} forbiddenWords={forbiddenWords} />
        </div>
        <div className="buttons">
          <Space direction="vertical">
            <Button
              className="guessed-word-btn"
              onClick={() => scoreRoundAsync(true)}
            >
              Word guessed!
            </Button>
            <Button
              type="ghost"
              className="skip-word-btn"
              onClick={() => scoreRoundAsync(false)}
            >
              Skip this word
            </Button>
          </Space>
        </div>
      </MainView>
    </div>
  );
};

export default ShowingPage;
