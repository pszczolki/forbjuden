import MainView from "@/components/mainView";
import TabooWord from "@/components/tabooWord";
import { useStores } from "@/store/provider";
import { Typography, Button } from "antd";
import React from "react";
import "./style.scss";

const { Title } = Typography;
type Props = TabooWordDto;

const Watching = (props: Props) => {
  const { scoreRoundAsync } = useStores();
  return (
    <div className="play-page">
      <MainView>
        <div>
          <Title type="secondary" level={5}>
            The oponent team is now trying to guess the taboo word. Watch if the
            forbidden words are not used.
          </Title>
          <TabooWord {...props} />
        </div>
        <div>
          <Button
            onClick={() => scoreRoundAsync(false)}
            type="primary"
            danger
            className="report-button"
          >
            Forbidden word used!
          </Button>
        </div>
      </MainView>
    </div>
  );
};

export default Watching;
