import MainView from "@/components/mainView";
import { Typography } from "antd";
import React from "react";

const { Title } = Typography;
const GuessingPage = () => {
  return (
    <div className="play-page">
      <MainView>
        <Title level={5}>Focus! Try to guess words!</Title>
      </MainView>
    </div>
  );
};

export default GuessingPage;
