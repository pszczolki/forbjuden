import { useStores } from "@/store/provider";
import { Button, Typography } from "antd";
import { observer } from "mobx-react-lite";
import React from "react";

interface Props {}

const { Title, Text } = Typography;

const EndedGamePage = (props: Props) => {
  const { yourPoints, enemyPoints, leaveRoomAsync } = useStores();

  const renderResult = () => {
    if (yourPoints > enemyPoints) {
      return (
        <Text>
          You won {yourPoints} to {enemyPoints}
        </Text>
      );
    } else if (enemyPoints > yourPoints) {
      return (
        <Text>
          You lost {yourPoints} to {enemyPoints}
        </Text>
      );
    } else {
      return <Text>It's a tie!</Text>;
    }
  };

  return (
    <div>
      <Title>Game finished</Title>
      {renderResult()}
      <div>
        <Button type="primary" danger onClick={leaveRoomAsync}>
          Leave game
        </Button>
      </div>
    </div>
  );
};

export default observer(EndedGamePage);
