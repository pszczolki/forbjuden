import React, { useState } from "react";
import Logo from "@/components/logo";
import { useStores } from "@/store/provider";
import { Typography, Space, Button, Row, Col, Input, Tabs } from "antd";
import { observer } from "mobx-react-lite";
import "./style.scss";

const { Text } = Typography;
const { TabPane } = Tabs;
const Home = () => {
  const [roomCode, setRoomCode] = useState("");
  const [roomPassword, setRoomPassword] = useState("");
  const { joinRoomAsync, createRoomAsync } = useStores();

  const joinRoomHandler = async () => {
    await joinRoomAsync(roomCode, roomPassword);
  };

  const createRoomHandler = async () => {
    await createRoomAsync();
  };

  const tabCreate = (
    <Row>
      <Col span={24}>
        <Space direction="vertical">
          <Text>Want to play with friends?</Text>
          <Button
            type="primary"
            style={{ width: "100%" }}
            onClick={createRoomHandler}
          >
            Create a new room!
          </Button>
        </Space>
      </Col>
    </Row>
  );

  const tabJoin = (
    <Row>
      <Col span={24}>
        <Space direction="vertical">
          <Text>Type a friend's room code.</Text>
          <Input
            onChange={e => setRoomCode(e.target.value)}
            placeholder="Room"
          />
          <Input
            onChange={e => setRoomPassword(e.target.value)}
            placeholder="Code"
          />
          <Button
            type="primary"
            style={{ width: "100%" }}
            onClick={joinRoomHandler}
          >
            Join
          </Button>
        </Space>
      </Col>
    </Row>
  );

  const tabs = (
    <Tabs defaultActiveKey="1">
      <TabPane tab={<span>Create room</span>} key="1">
        {tabCreate}
      </TabPane>
      <TabPane tab={<span>Join room</span>} key="2">
        {tabJoin}
      </TabPane>
    </Tabs>
  );

  return (
    <div className="main-page">
      <Row className="logo">
        <Col span={24}>
          <Logo />
        </Col>
      </Row>
      <Row>
        <Col span={24}>{tabs}</Col>
      </Row>
    </div>
  );
};

export default observer(Home);
