import { useStores } from "@/store/provider";
import { Button, Space } from "antd";
import { observer } from "mobx-react-lite";
import React, { useState } from "react";
import "./style.scss";

const WaitingRoom = () => {
  const {
    isUserReady,
    roomId,
    code,
    users,
    leaveRoomAsync,
    setReadyForRoomAsync: setReadyForRoom
  } = useStores();
  const [disable, setDisable] = useState(false);

  const setReady = () => {
    setDisable(true);
    setReadyForRoom().finally(() => setDisable(false));
  };
  return (
    <>
      <header className="lobby-header-info">
        <em>Minimum 4 players are needed.</em>
        <em>Game will start automatically when every player is ready.</em>
      </header>
      <p>{`Room: ${roomId}`}</p>
      <p>{`Code: ${code}`}</p>
      <div>Waiting for other players...</div>
      <ul>
        {users.map(u => (
          <li key={u.userId}>
            {u.userId} <span>{u.readyToPlay ? "✔" : "❌"}</span>
          </li>
        ))}
      </ul>
      <Space>
        <Button
          type="primary"
          disabled={disable || isUserReady}
          onClick={setReady}
        >
          Set ready
        </Button>
        <Button type="primary" danger onClick={leaveRoomAsync}>
          Leave
        </Button>
      </Space>
    </>
  );
};

export default observer(WaitingRoom);
