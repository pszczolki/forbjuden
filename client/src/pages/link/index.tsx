import Logo from "@/components/logo";
import { Typography, Space, Button, Row, Col, Input } from "antd";
import React, { Component } from "react";

const { Text, Title } = Typography;

const Home = () => {
  return (
    <div>
      <Logo />

      <Space direction="vertical">
        <Text>Type a friend's room code.</Text>
        <Input placeholder="code" />
        <Input placeholder="password" />
        <Button>Join</Button>
      </Space>
    </div>
  );
};

export default Home;
