import React from 'react';

interface Props {}

const AbortedGamePage = (props: Props) => {
  return <div>Game was aborted</div>;
};

export default AbortedGamePage;
