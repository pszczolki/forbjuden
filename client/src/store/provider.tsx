import React, { useContext } from 'react';
// eslint-disable-next-line no-unused-vars
import { useLocalObservable } from 'mobx-react-lite';
import roomStore, { RoomStore } from '.';

const createContext = () => roomStore;
const StoreContext = React.createContext<RoomStore>(createContext());

interface Props {
  children: JSX.Element;
}

export const StoreProvider = ({ children }: Props): JSX.Element => {
  const store = useLocalObservable(createContext);

  return (
    <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
  );
};

export const useStores = (): RoomStore => useContext(StoreContext)!;
