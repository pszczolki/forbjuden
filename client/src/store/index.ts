import {
  createRoomAsync,
  getUserInfoAsync,
  joinRoomAsync,
  leaveRoomAsync,
  gameDetailsAsync,
  getLobbyAsync,
  setRoomReadyAsync,
  setReadyForRoundAsync,
  scoreRoundAsync,
  endRoundAsync,
  abortGameAsync
} from "@/api";
import { WebSocketService } from "@/ws";
import { throws } from "assert";
import { computed, makeAutoObservable, observable } from "mobx";

interface TabooEntry {
  guessingWord: string;
  tabooWords: string[];
  tabooGuid: string;
}

export class RoomStore {
  private _isLoading: boolean;
  apiInstance: string;
  wsInstance: string;
  yourPoints: number;
  enemyPoints: number;
  teamName: string;
  private _roomId: string;
  private _code: string;
  private _userId: string;
  private _round: number;
  private _roundStarted: boolean;
  private _roundEndDate?: Date;
  private _gameStarted: boolean;
  private _gameEnded: boolean;
  private _gameAborted: boolean;
  private _users: ReadyUser[];
  private _wsService: WebSocketService;
  private _areYouShowing: boolean;
  private _isYourTeamGuessing: boolean;
  private _taboo?: TabooEntry;

  constructor() {
    this.setCode = this.setCode.bind(this);
    this.setRound = this.setRound.bind(this);
    this.setRoomId = this.setRoomId.bind(this);
    this.setUserId = this.setUserId.bind(this);
    this.setUsers = this.setUsers.bind(this);

    this.abortGameAsync = this.abortGameAsync.bind(this);
    this.leaveRoomAsync = this.leaveRoomAsync.bind(this);
    this.joinRoomAsync = this.joinRoomAsync.bind(this);
    this.createRoomAsync = this.createRoomAsync.bind(this);
    this.wsResolver = this.wsResolver.bind(this);

    this._isLoading = true;
    this.apiInstance = "";
    this.teamName = "";
    this.wsInstance = "";
    this.yourPoints = 0;
    this.enemyPoints = 0;
    this._areYouShowing = false;
    this._isYourTeamGuessing = false;
    this._roundStarted = false;
    this._gameStarted = false;
    this._gameEnded = false;
    this._gameAborted = false;
    this._roomId = "";
    this._userId = "";
    this._code = "";
    this._round = 0;
    this._users = observable.array();
    this._wsService = new WebSocketService(this.wsResolver);
    this._wsService.keepConnectionAsync();
    makeAutoObservable(this);
    this.setGameDetailsAsync();
  }

  wsResolver(message: SocketMessage) {
    console.log("Type", message.type, "Data", message.data);
    switch (message.type) {
      case "room_joined":
      case "room_left":
      case "room_ready":
        this._onLobbyChangeMessageAsync();
        break;
      case "game_aborted":
        this._onGameAbortedMessageAsync();
        break;
      case "game_ended":
        this._onGameEndedMessageAsync();
        break;
      case "game_started":
        this._onGameStartedMessageAsync();
        break;
      case "round_ended":
        this._onRoundEndedMessageAsync();
        break;
      case "round_started":
        this._onRoundStartedMessageAsync();
        break;
      case "taboo_changed":
        this._onTabooPhraseChanged();
        break;
      case "ws_connected":
        this.wsInstance = message.data;
        break;
      case "ws_disconnected":
        console.log("WS disconnected from instance:", this.wsInstance);
        this.wsInstance = "";
        break;
      default:
        break;
    }
  }

  setGameDetailsAsync = async () => {
    this._isLoading = true;
    const { success, data, errors } = await gameDetailsAsync();
    if (success) {
      const { code, players, roomId, game } = data!;
      this.setRoomId(roomId);
      this.setCode(code);
      this.setUsers(players);
      if (game != null) {
        this.teamName = game.teamName;
        this._gameStarted = true;
        this._areYouShowing = game.areYouShowing;
        this._isYourTeamGuessing = game.isYourTeamGuessing;
        this._gameEnded = game.gameEnded;
        this._round = game.round;
        this._roundStarted = game.roundStarted;
        this._roundEndDate = game.roundEndDate;
        this.enemyPoints = game.points.enemyTeam;
        this.yourPoints = game.points.yourTeam;

        if (game.taboo) {
          this._taboo = {
            guessingWord: game.taboo.guessingWord,
            tabooWords: game.taboo.tabooWords,
            tabooGuid: game.taboo.tabooGuid
          };
        }
      }
    } else {
      console.error(errors);
    }
    this._isLoading = false;
  };

  setReadyForNextRoundAsync = async () => {
    await setReadyForRoundAsync();
    await this.setGameDetailsAsync();
  };

  setReadyForRoomAsync = async () => {
    await setRoomReadyAsync();
    await this.setGameDetailsAsync();
  };

  scoreRoundAsync = async (guessed: boolean) => {
    await scoreRoundAsync(guessed, this._taboo?.tabooGuid || "");
  };

  // websocket message handlers
  _resetState = () => {
    this._gameAborted = false;
    this._gameStarted = false;
    this._gameEnded = false;
    this._roundStarted = false;
    this._roundEndDate = undefined;
    this._round = 0;
    this._users = [];
    this._roomId = "";
    this._code = "";
    this._taboo = undefined;
  };

  _onLobbyChangeMessageAsync = async () => {
    const { success, data, errors } = await getLobbyAsync();
    if (success) {
      this.setUsers(data!.users);
    } else {
      console.error(errors);
    }
  };

  _onGameAbortedMessageAsync = async () => {
    this._resetState();
    // this._gameAborted = true;
  };

  _onGameEndedMessageAsync = async () => {
    this._resetState();
    this.setGameDetailsAsync();
  };

  _onGameStartedMessageAsync = async () => {
    this.setGameDetailsAsync();
  };

  _onRoundEndedMessageAsync = async () => {
    this.setGameDetailsAsync();
  };

  _onRoundStartedMessageAsync = async () => {
    this.setGameDetailsAsync();
  };

  _onTabooPhraseChanged = async () => {
    this.setGameDetailsAsync();
  };

  setCode(code: string) {
    this._code = code;
  }

  setRoomId(roomId: string) {
    this._roomId = roomId;
  }

  setUserId(userId: string) {
    this._userId = userId;
  }

  setUsers(users: ReadyUser[]) {
    this.users.splice(0, this.users.length);
    this.users.push(...users);
  }

  setRound(round: number) {
    this._round = round;
  }

  endRoundAsync = async () => {
    await endRoundAsync(this.round);
  };

  async abortGameAsync() {
    const { success, errors } = await abortGameAsync();
    if (success) {
      this._resetState();
    } else {
      console.error(errors);
    }
  }

  async leaveRoomAsync() {
    const { success, errors } = await leaveRoomAsync();
    if (success) {
      this._resetState();
    } else {
      console.error(errors);
    }
  }

  async joinRoomAsync(roomCode: string, roomPassword: string) {
    const { success, data, errors } = await joinRoomAsync(roomCode, {
      code: roomPassword
    });
    if (success) {
      const { users, roomId, code } = data!;
      this.setDetails(users, roomId, code);
    } else {
      console.error(errors);
    }
  }

  async createRoomAsync() {
    const { success, data, errors } = await createRoomAsync();
    if (success) {
      const { users, roomId, code } = data!;
      this.setDetails(users, roomId, code);
    } else {
      console.error(errors);
    }
  }

  getDetailsAsync = async () => {
    this._isLoading = true;
    const { success, data, errors } = await getUserInfoAsync();
    if (success) {
      const { userId, instanceGuid } = data!;
      this.setUserId(userId);
      this.apiInstance = instanceGuid;
    } else {
      console.error(errors);
    }
    this._isLoading = false;
  };

  setDetails = (users: ReadyUser[], roomId: string, code: string) => {
    this.setUsers(users);
    this.setRoomId(roomId);
    this.setCode(code);
  };

  get hasRoom(): boolean {
    return this._roomId !== "" || this._code !== "";
  }

  get userId(): string {
    return this._userId;
  }

  get users(): ReadyUser[] {
    return this._users;
  }

  get roomId(): string {
    return this._roomId;
  }

  get round(): number {
    return this._round;
  }

  get code(): string {
    return this._code;
  }

  get gameStarted(): boolean {
    return this._gameStarted;
  }

  get gameEnded(): boolean {
    return this._gameEnded;
  }

  get gameAborted(): boolean {
    return this._gameAborted;
  }

  get roundStarted(): boolean {
    return this._roundStarted;
  }

  get getRoundEndDate(): Date | undefined {
    return this._roundEndDate;
  }

  get areYouShowing(): boolean {
    return this._areYouShowing;
  }

  get isYourTeamGuessing(): boolean {
    return this._isYourTeamGuessing;
  }

  get taboo(): TabooEntry | undefined {
    return this._taboo;
  }

  get isUserReady(): boolean {
    return this._users
      .filter(u => u.readyToPlay)
      .map(u => u.userId)
      .includes(this._userId);
  }

  get isLoading(): boolean {
    return this._isLoading && !this.hasRoom;
  }
}

const roomStore = new RoomStore();

export default roomStore;
