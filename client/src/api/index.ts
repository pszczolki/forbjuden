import Axios from "axios";

export const apiOrigin =
  process.env.REACT_APP_API_URL || "http://localhost:5050";

const urls = {
  info: `${apiOrigin}/info`,
  room: {
    create: `${apiOrigin}/room/create`,
    join: (roomId: string) => `${apiOrigin}/room/join/${roomId}`,
    leave: `${apiOrigin}/room/leave`,
    lobby: `${apiOrigin}/room/lobby`,
    ready: `${apiOrigin}/room/ready`
  },
  round: {
    score: `${apiOrigin}/round/score`,
    end: `${apiOrigin}/round/end`,
    ready: `${apiOrigin}/round/ready`
  },
  game: {
    details: `${apiOrigin}/game`,
    abort: `${apiOrigin}/game/abort`
  }
};

const TIMEOUT = 10_000_000;

interface DetailsApiResponse {
  token: string;
  userId: string;
  instanceGuid: string;
}
interface ApiResponse<T> {
  success: boolean;
  data?: T;
  errors?: string[];
}

interface JoinRoomApiResponse {
  users: ReadyUser[];
  roomId: string;
  code: string;
}
interface JoinRoomApiRequest {
  code: string;
}

interface LeaveRoomApiResponse {}

interface LobbyApiResponse {
  users: ReadyUser[];
  code: string;
  roomId: string;
}

type TabooTeamEnum = "teamA" | "teamB";

interface TabooEntry {
  guessingWord: string;
  tabooWords: string[];
  tabooGuid: string;
}

interface CommonDetailsApiResponse {
  roomId: string;
  code: string;
  players: RoomPlayerApiResponse[];
  game?: GameDetailsApiResponse;
}

interface RoomPlayerApiResponse {
  userId: string;
  readyToPlay: boolean;
}

interface GameDetailsApiResponse {
  teamName: string;
  isYourTeamGuessing: boolean;
  areYouShowing: boolean;
  points: {
    yourTeam: number;
    enemyTeam: number;
  };
  gameEnded: boolean;
  round: number;
  roundStarted: boolean;
  roundEndDate?: Date;
  taboo?: TabooEntry;
}

export const getUserInfoAsync = async () => {
  return await _getAsync<DetailsApiResponse>(urls.info);
};

export const joinRoomAsync = async (roomId: string, data: JoinRoomApiRequest) =>
  await _postAsync<JoinRoomApiResponse>(urls.room.join(roomId), data);

export const createRoomAsync = async () =>
  await _postAsync<JoinRoomApiResponse>(urls.room.create);

export const leaveRoomAsync = async () =>
  await _postAsync<LeaveRoomApiResponse>(urls.room.leave);

export const getLobbyAsync = async () =>
  await _getAsync<LobbyApiResponse>(urls.room.lobby);

export const setRoomReadyAsync = async () =>
  await _postAsync<boolean>(urls.room.ready, { ready: true });

export const abortGameAsync = async () => _postAsync<void>(urls.game.abort);

export const gameDetailsAsync = async () =>
  _getAsync<CommonDetailsApiResponse>(urls.game.details);

export const endRoundAsync = async (round: number) =>
  await _postAsync<void>(urls.round.end, { round: round });

export const scoreRoundAsync = async (guessed: boolean, tabooGuid: string) =>
  await _postAsync(urls.round.score, { guessed, tabooGuid });

export const setReadyForRoundAsync = async () => {
  await _postAsync<void>(urls.round.ready, { ready: true });
};

const _getAsync = async <T>(url: string): Promise<ApiResponse<T>> => {
  try {
    const { data, status } = await Axios.get<T>(url, {
      withCredentials: true,
      timeout: TIMEOUT
    });

    if (status === 200) {
      return {
        success: true,
        data: data
      };
    } else {
      throw { error: "Invalid status code", code: status };
    }
  } catch (err) {
    return { success: false, errors: [err.toString()] };
  }
};

const _postAsync = async <T>(url: string, request: any = null) => {
  try {
    const { data, status } = await Axios.post<T>(url, request, {
      withCredentials: true,
      timeout: TIMEOUT
    });

    if (status === 200) {
      return {
        success: true,
        data: data
      };
    } else {
      throw { error: "Invalid status code", code: status };
    }
  } catch (err) {
    return { success: false, errors: [err.toString()] };
  }
};
