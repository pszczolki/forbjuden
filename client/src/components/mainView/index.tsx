import TimeLeft from "@/components/timeLeft";
import { useStores } from "@/store/provider";
import { Button, Col, Divider, Row, Space } from "antd";
import { observer } from "mobx-react-lite";
import React, { useEffect, useRef, useState } from "react";
import Points from "../points";
import moment from "moment";

const MainView = ({ children }: any) => {
  const { getRoundEndDate, endRoundAsync } = useStores();
  const [time, setTime] = useState(60); // cant be 0, otherwise glitches
  const timeIsUp = time <= 0;
  const interval = useRef<any>(null);

  useEffect(() => {
    if (getRoundEndDate == null) {
      return;
    }

    const endAtMoment = moment(getRoundEndDate);
    const duration = moment.duration(endAtMoment.diff(moment.utc()));
    const secondsUntilEnd = Math.floor(duration.asSeconds());

    setTime(secondsUntilEnd);
    if (interval.current) {
      clearTimeout(interval.current);
    }
    interval.current = setInterval(() => {
      if (time <= 0) {
        clearTimeout(interval.current);
      } else {
        setTime(t => t - 1);
      }
    }, 1000);

    return () => {
      clearInterval(interval.current);
    };
  }, [getRoundEndDate]);

  return (
    <Space direction="vertical">
      <Space>
        <Points />
      </Space>
      <Divider />

      <Space>
        <TimeLeft timeLeft={time!} />
      </Space>
      {!timeIsUp && <Space direction="vertical">{children}</Space>}
      <Divider />
      <Space>
        <Button type="primary" danger onClick={endRoundAsync}>
          End round
        </Button>
      </Space>
    </Space>
  );
};

export default observer(MainView);
