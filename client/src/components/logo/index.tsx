import { Typography, Space, Button, Row, Col, Input } from "antd";
import React, { Component } from "react";
import "./style.scss";

const { Title } = Typography;

const Logo = () => {
  return (
    <div className="logo">
      <Space direction="vertical">
        <Title level={1}>Förbjuden</Title>
        <Title level={5} type="secondary">
          Online taboo game 🤫
        </Title>
      </Space>
    </div>
  );
};

export default Logo;
