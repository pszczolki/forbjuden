import { Typography, Space } from "antd";
import React from "react";

const { Text, Title } = Typography;

type Props = ForbiddenWordsDto;

const ForbiddenWords = ({ forbiddenWords }: Props) => {
  return (
    <div>
      <Space direction="vertical">
        {forbiddenWords.map(w => (
          <Text key={w} type="danger">
            {w}
          </Text>
        ))}
      </Space>
    </div>
  );
};

export default ForbiddenWords;
