import { useStores } from "@/store/provider";
import { Space, Typography } from "antd";
import { observer } from "mobx-react-lite";
import React from "react";

const { Text } = Typography;

const Points = () => {
  const { yourPoints, enemyPoints, round, teamName } = useStores();
  return (
    <Space direction="horizontal" align="start">
      <Text code>Team: {teamName}</Text>
      <Text code>Round: {round}</Text>
      <Space direction="vertical">
        <Text code>Your Points: {yourPoints}</Text>
        <Text code>Enemy Points: {enemyPoints}</Text>
      </Space>
    </Space>
  );
};

export default observer(Points);
