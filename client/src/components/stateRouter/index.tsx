import { useStores } from "@/store/provider";
import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom";

const StateRouter = () => {
  const { pathname } = useLocation();
  const { push } = useHistory();
  const {
    hasRoom,
    gameAborted,
    gameEnded,
    gameStarted,
    roundStarted
  } = useStores();

  useEffect(() => {
    if (hasRoom && !gameStarted) {
      if (pathname != "/lobby") {
        push("/lobby");
      }
    }
  }, [hasRoom]);

  // useEffect(() => {
  //   if (gameAborted) {
  //     if (pathname != '/game/aborted') {
  //       push('/game/aborted');
  //     }
  //   }
  // }, [gameAborted]);

  useEffect(() => {
    if (gameEnded) {
      if (pathname != "/game/ended") {
        push("/game/ended");
      }
    }
  }, [gameEnded]);

  useEffect(() => {
    if (gameStarted && !roundStarted) {
      if (pathname != "/game/lobby") {
        push("/game/lobby");
      }
    } else if (gameStarted && roundStarted) {
      if (pathname != "/game/play") {
        push("/game/play");
      }
    }
  }, [gameStarted, roundStarted]);

  useEffect(() => {
    if (!hasRoom && !gameStarted) {
      push("/home");
    }
  }, [hasRoom, gameStarted]);

  return <></>;
};

export default observer(StateRouter);
