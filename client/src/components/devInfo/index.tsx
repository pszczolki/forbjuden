import { useStores } from "@/store/provider";
import { Button, Popover } from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";
import { observer } from "mobx-react-lite";
import React, { useState } from "react";
import "./style.scss";

interface Props {}

const DevInfo = (props: Props) => {
  const [visible, show] = useState(false);
  const { wsInstance, apiInstance, userId, roomId, code } = useStores();

  const devInfo = (
    <table className="dev-info__table">
      <tbody>
        <tr>
          <td>Api instance:</td>
          <td>{apiInstance}</td>
        </tr>
        <tr>
          <td>WS instance:</td>
          <td>{wsInstance}</td>
        </tr>
        <tr>
          <td>User ID:</td>
          <td>{userId}</td>
        </tr>
        <tr>
          <td>Room ID:</td>
          <td>{roomId}</td>
        </tr>
        <tr>
          <td>Room Code:</td>
          <td>{code}</td>
        </tr>
      </tbody>
    </table>
  );

  return (
    <div className="dev-info">
      <div className="dev-info__container">
        <Popover
          placement="bottomLeft"
          content={devInfo}
          trigger="click"
          visible={visible}
        >
          <Button
            icon={<InfoCircleOutlined />}
            type="primary"
            onClick={() => show(prev => !prev)}
          ></Button>
        </Popover>
      </div>
    </div>
  );
};

export default observer(DevInfo);
