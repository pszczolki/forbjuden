import { Progress } from "antd";
import React from "react";

const totalTime = 60; // hardcoded in api
interface Props {
  timeLeft: number;
}
const TimeLeft = ({ timeLeft }: Props) => {
  const time = Math.floor(timeLeft);

  function getTimeString(): string {
    if (time > 9) {
      return `0:${time}`;
    } else if (time > 0) {
      return `0:0${time}`;
    } else {
      return "Time is up!";
    }
  }

  return (
    <div>
      <Progress
        type="circle"
        percent={(time / totalTime) * 100}
        format={() => `${getTimeString()}`}
        status="normal"
        strokeColor={{
          "0%": "#a61d24",
          "100%": "#87d068"
        }}
      />
    </div>
  );
};

export default TimeLeft;
