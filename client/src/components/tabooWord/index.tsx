import { Typography, Space } from "antd";
import React from "react";
import ForbiddenWords from "../forbiddenWords";

const { Title } = Typography;

type Props = TabooWordDto;

const TabooWord = ({ word, forbiddenWords }: Props) => {
  return (
    <div>
      <Space direction="vertical">
        <Title level={1}>{word}</Title>
        <ForbiddenWords forbiddenWords={forbiddenWords} />
      </Space>
    </div>
  );
};

export default TabooWord;
