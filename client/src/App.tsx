import React, { useEffect } from "react";
import "./App.scss";
import "antd/dist/antd.dark.min.css";
import { BrowserRouter, Redirect, Route } from "react-router-dom";
import HomePage from "./pages/home";
import { StoreProvider, useStores } from "./store/provider";
import StateRouter from "@/components/stateRouter";
import RoomLobbyPage from "./pages/waitRoom";
import { observer } from "mobx-react-lite";
import PlayPage from "./pages/play";
import RoundReadyPage from "./pages/roundReady";
import EndedGamePage from "./pages/gameEnded";
import AbortedGamePage from "./pages/gameAborted";
import DevInfo from "./components/devInfo";

function App() {
  const { getDetailsAsync, isLoading } = useStores();

  useEffect(() => {
    getDetailsAsync();
  }, []);

  const loadingWrapper = (component: any) =>
    isLoading ? () => <>Loading...</> : component;

  return (
    <div className="App">
      <StoreProvider>
        <>
          <DevInfo />
          {/* {process.env.NODE_ENV == "development" && <DevInfo />} */}
          <BrowserRouter>
            <StateRouter />
            <Route path="/home" exact component={loadingWrapper(HomePage)} />
            <Route
              path="/lobby"
              exact
              component={loadingWrapper(RoomLobbyPage)}
            />
            <Route
              path="/game/lobby"
              exact
              component={loadingWrapper(RoundReadyPage)}
            />
            <Route
              path="/game/play"
              exact
              component={loadingWrapper(PlayPage)}
            />
            <Route
              path="/game/ended"
              exact
              component={loadingWrapper(EndedGamePage)}
            />
            <Route
              path="/game/aborted"
              exact
              component={loadingWrapper(AbortedGamePage)}
            />
            <Redirect to="/home" />
          </BrowserRouter>
        </>
      </StoreProvider>
    </div>
  );
}

export default observer(App);
