interface TabooWordDto extends ForbiddenWordsDto {
  word: string;
}

interface ForbiddenWordsDto {
  forbiddenWords: string[];
}

interface ReadyUser {
  userId: string;
  readyToPlay: boolean;
}
