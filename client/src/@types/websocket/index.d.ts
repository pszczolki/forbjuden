type MessageType =
  | "ws_connected"
  | "ws_disconnected"
  | "room_joined"
  | "room_left"
  | "room_ready"
  | "game_started"
  | "round_started"
  | "round_ended"
  | "game_ended"
  | "game_aborted"
  | "taboo_changed";

interface SocketMessage {
  type: MessageType;
  data: any;
}
