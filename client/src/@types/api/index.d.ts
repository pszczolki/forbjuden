interface DetailsApiResponse {
  token: string;
  user: ReadyUser;
  roomId?: string;
}
interface ApiResponse<T> {
  success: boolean;
  data?: T;
  errors?: string[];
}

interface JoinRoomApiResponse {
  users: ReadyUser[];
  roomId: string;
  code: string;
}
interface JoinRoomApiRequest {
  code: string;
}

interface LeaveRoomApiResponse {}

type TabooTeamEnum = 'teamA' | 'teamB';

interface TabooEntry {
  guessingWord: string;
  tabooWords: string[];
}

interface CommonDetailsApiResponse {
  roomId: string;
  code: string;
  players: RoomPlayerApiResponse[];
  game?: GameDetailsApiResponse;
}

interface RoomPlayerApiResponse {
  userId: string;
  readyToPlay: boolean;
}

interface GameDetailsApiResponse {
  isYourTeamGuessing: boolean;
  areYouShowing: boolean;
  points: {
    teamA: number;
    teamB: number;
  };
  round: number;
  roundStarted: boolean;
  roundStartDate?: Date;
  taboo?: TabooEntry;
}
