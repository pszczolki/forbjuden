import { getUserInfoAsync } from "@/api";

const wsServer = process.env.REACT_APP_WS_URL || "ws://localhost:7070";

export class WebSocketService {
  private ws: WebSocket | undefined = undefined;
  private interval: any;
  constructor(private resolver: (message: SocketMessage) => void) {
    this.interval = null;
  }

  keepConnectionAsync = async () => {
    try {
      this.ws = await this._connectAsync();
      this.ws?.addEventListener(
        "close",
        async () => await this.keepConnectionAsync()
      );

      setTimeout(this._setIntervalStateChecked, 500);
    } catch (error) {
      console.error(error);
      this.dispose();
      this._retryConnection();
    }
  };

  _setIntervalStateChecked = () => {
    if (!this.interval) {
      this.interval = setInterval(() => {
        if (
          this.ws?.readyState !== this.ws?.OPEN &&
          this.ws?.readyState !== this.ws?.CONNECTING
        ) {
          console.log(
            `Connection is NOT alive with state: ${this.ws?.readyState}. Reconnecting...`
          );
          this.keepConnectionAsync();
        }
      }, 1000);
    }
  };

  _retryConnection = () => {
    const waitTime = 100;
    console.log(`Waiting ${waitTime}ms for WebSocket reconnection`);

    setTimeout(async () => {
      await this.keepConnectionAsync();
    }, waitTime);
  };

  _connectAsync = async () => {
    const { success, errors, data } = await getUserInfoAsync();
    const ws = new WebSocket(wsServer);
    if (success) {
      ws.addEventListener("open", () => {
        ws.send(data!.token);
      });

      ws.addEventListener("message", ({ data }) => {
        const message = JSON.parse(data) as SocketMessage;
        this.resolver(message);
      });

      ws.addEventListener("close", () => {
        this.resolver({ type: "ws_disconnected", data: null });
      });

      return ws;
    } else {
      throw errors;
    }
  };

  dispose = () => {
    clearInterval(this.interval);
    this.ws?.close();
  };
}
