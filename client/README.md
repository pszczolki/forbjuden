## About 

Client side application for taboo web game. For proper behavior all of the other services: websocket, session api and phrases api must be working correctly.

## How to run

Project is supposed to run with all the other microservies in kubernetes (too see setup instruction go to `/k8s/README.md` but can be also run as a standalone app:
1. In docker. 
- to build image run `docker build -t <image-name>`
- to run created image run `docker run -p <destination-port>:3000 --rm -d <image-name>`
2. Locally:
- Run `npm install` / `yarn install`
- Edit `.env.development` file variables to correspond with other created microservice urls.  
- Run `npm start` / `yarn start`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build` & `npm run build:server` 

Builds the app for production to the `build` and `build-server` folders.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.
- `build` folder consist of bundled web client files
- `build-server` folder contains server.js file used fort serving web client files (.js, .html, etc.)

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
