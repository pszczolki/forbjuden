# About
Folder contains 2 services: websocket and session api
1. websocket
Used for game synchronization 

2. sessions api
Used for game logic

# How to run locally
- Ensure Redis is running, also can be run with docker by `run_redis.sh`
- Update .env file
- install yarn
- run `yarn install`
- Edit `.env` file with proper values
- run `yarn run`

For setting up whole environment kubernetes setup is preferred. See `/k8s/README.md`

# Docker commands

## Image builds

- API

`docker build -t forbjuden-session-api:<version> -f dockerfile.api .`

- WS

`docker build -t forbjuden-websocket:<version> -f dockerfile.ws .`

## Container run

### API

`docker run -p <port>:5050 -e REDIS_PORT=<redis-port> -e REDIS_HOST=<redis-host> forbjuden-session-api:<version>`

- `-e REDIS_HOST=host.docker.internal` for local redis

### WS

`docker run -p <port>:7070 -e REDIS_PORT=<redis-port> -e REDIS_HOST=<redis-host> forbjuden-websocket:<version>`

- `-e REDIS_HOST=host.docker.internal` for local redis
