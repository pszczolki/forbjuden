import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import {
  createRoom,
  createSessionMiddleware,
  endRoundAsync,
  getDetails,
  joinRoom,
  leaveRoom,
  getLobby,
  setLobbyReadyAsync,
  abortGameAsync,
  getGameDetailsAsync,
  userIdMiddleware,
  setRoundReadyAsync,
  scorePointOnRoundAsync
} from './session';
import { makeGuid, normalizePort } from './utility';

const instanceGuid = `api_${makeGuid(5)}`;
const apiPort = normalizePort(process.env.PORT || 5050);
const clientOrigin = process.env.CLIENT_ORIGIN || 'http://localhost:3000';

const app = express();
app.use(cors({ origin: clientOrigin, credentials: true }));
app.use(createSessionMiddleware());
app.use(userIdMiddleware);
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

app.get('/', (req, resp) => resp.json('OK'));

app.get('/counter', (req, resp) => {
  const session = req.session as any;
  session.cookie;
  if (session.counter == null) {
    session.counter = 0;
  }

  session.counter++;
  resp.json({ counter: session.counter });
});

app.post('/room/create', async (req, resp) => {
  const response = await createRoom(req.session as any);
  resp.json(response);
});

app.post('/room/join/:id', async (req, resp) => {
  const id = req.params.id as string;
  const code = req.body.code as string;
  resp.json(await joinRoom(req.session as any, id, code));
});

app.post('/room/leave', async (req, resp) => {
  resp.json(await leaveRoom(req.session as any));
});

app.get('/info', async (req, resp) => {
  resp.json({ ...(await getDetails(req.session as any)), instanceGuid });
});

app.post('/room/ready', async (req, resp) => {
  const ready = req.body.ready as boolean;
  resp.json(await setLobbyReadyAsync(req.session as any, ready));
});

app.get('/room/lobby', async (req, resp) => {
  resp.json(await getLobby(req.session as any));
});

app.post('/round/score', async (req, resp) => {
  responseWrapperAsync(resp, async () => {
    const guessed = req.body.guessed as boolean;
    const tabooGuid = req.body.tabooGuid as string;
    resp.json(
      await scorePointOnRoundAsync(req.session as any, guessed, tabooGuid)
    );
  });
});

app.post('/round/end', async (req, resp) => {
  const round = req.body.round as number;
  resp.json(await endRoundAsync(req.session as any, round));
});

app.post('/round/ready', async (req, resp) => {
  const ready = req.body.ready as boolean;
  resp.json(await setRoundReadyAsync(req.session as any, ready));
});

app.post('/game/abort', async (req, resp) => {
  responseWrapperAsync(resp, async () => {
    resp.json(await abortGameAsync(req.session as any));
  });
});

app.get('/game', async (req, resp) => {
  responseWrapperAsync(resp, async () => {
    const data = await getGameDetailsAsync(req.session as any);
    resp.json(data);
  });
});

const responseWrapperAsync = async (resp: any, action: () => Promise<any>) => {
  try {
    return await action();
  } catch (error) {
    console.error(error);
    resp.status(400);
    resp.send(error);
  }
};

app.listen(apiPort, '0.0.0.0');

export default {};
