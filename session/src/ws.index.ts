import dotenv from 'dotenv';
dotenv.config();
import { normalizePort } from './utility';
import { createWSServer } from './websocket';

const wsPort = normalizePort(process.env.WS_PORT || 7070);

const wsServer = createWSServer(wsPort);

export default {};
