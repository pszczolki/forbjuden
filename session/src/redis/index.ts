import redis, { RedisClient } from 'redis';
import { sendToUser } from '../websocket';

export const REDIS_PORT = Number(process.env.REDIS_PORT || 6379);

export const REDIS_HOST = process.env.REDIS_HOST || 'localhost';

export const redisClient = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT
});

redisClient.on('error', (err: any) => {
  console.log('REDIS error: ', err);
});

const subscriber = redisClient.duplicate();
const publisher = subscriber.duplicate();
const WS_CHANNEL = 'ws:messages';

subscriber.on('message', (channel, ms) => {
  console.log(`Message from: ${channel}, ${ms}`);
  const { message, userId } = JSON.parse(ms) as BrodcastModel;
  sendToUser(userId, message);
});

subscriber.subscribe(WS_CHANNEL);

export const sendBroadcastedWsMessages = (...messages: BrodcastModel[]) => {
  console.log('Broadcasting messages:');
  messages.forEach((m) => {
    const stringified = JSON.stringify(m);
    console.log('Stringified message: ', stringified);
    publisher.publish(WS_CHANNEL, JSON.stringify(m));
  });
};

const getAsync = (redis: RedisClient, key: string): Promise<string | null> => {
  return new Promise((resolve, reject) => {
    redis.get(key, (error, value) => {
      if (error) {
        return reject(error);
      } else {
        resolve(value);
      }
    });
  });
};

export const getValueAsync = async (key: string): Promise<string | null> =>
  await getAsync(redisClient, key);

export const setValueAsync = async (
  key: string,
  value: string
): Promise<boolean> => await setAsync(redisClient, key, value);

export const deleteValueAsync = async (key: string): Promise<boolean> =>
  await deleteAsync(redisClient, key);

const setAsync = (
  redis: RedisClient,
  key: string,
  value: string
): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    redis.set(key, value, (error, reply) => {
      if (error) {
        return reject(error);
      } else {
        return resolve(reply == 'OK');
      }
    });
  });
};

const deleteAsync = (redis: RedisClient, key: string): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    redis.del(key, (error, reply) => {
      console.log('error', error, 'reply', reply);
      if (error) {
        return reject(error);
      } else {
        return resolve(reply > 0);
      }
    });
  });
};
