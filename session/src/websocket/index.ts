import { sendBroadcastedWsMessages } from '../redis';
import WebSocket, { Server } from 'ws';
import { checkToken, makeGuid } from '../utility';
import { getRoomSessionAsync } from '../session';

interface ConnectedUser extends WebSocket {
  userId: string;
  roomId: string;
}

const instanceGuid = `ws_${makeGuid(5)}`;
let connectedUsers: ConnectedUser[] = [];

const onOpen = (ws: WebSocket) => {
  const onOpenMessage: SocketMessage = {
    type: 'ws_connected',
    data: instanceGuid
  };
  ws.send(JSON.stringify(onOpenMessage));
};

const onClose = async (ws: WebSocket) => {
  const userWS = ws as ConnectedUser;
  connectedUsers = connectedUsers.filter((s) => s.userId != userWS.userId);
  console.log('Disconnected userId: ', userWS.userId || '{NO ID ERROR}');
};

export const getConnectedUsers = () => connectedUsers;

export const getConnectedUser = (userId: string) =>
  connectedUsers.find((w) => w.userId == userId);

export const sendToUser = (userId: string, message: SocketMessage) => {
  const user = connectedUsers.find((u) => u.userId == userId);
  if (user) {
    user.send(JSON.stringify(message));
    console.log(`Send message: ${message.type} to user: ${userId}`);
  } else {
    console.log(`FAILED message: ${message.type} to user: ${userId}`);
  }
};

export const createWSServer = (port: number) => {
  const server = new Server({
    host: '0.0.0.0',
    port,
    perMessageDeflate: {
      zlibDeflateOptions: {
        // See zlib defaults.
        chunkSize: 1024,
        memLevel: 7,
        level: 3
      },
      zlibInflateOptions: {
        chunkSize: 10 * 1024
      },
      // Other options settable:
      clientNoContextTakeover: true, // Defaults to negotiated value.
      serverNoContextTakeover: true, // Defaults to negotiated value.
      serverMaxWindowBits: 10, // Defaults to negotiated value.
      // Below options specified as default values.
      concurrencyLimit: 10, // Limits zlib concurrency for perf.
      threshold: 1024 // Size (in bytes) below which messages
      // should not be compressed.
    }
  });

  server.on('connection', function connection(ws) {
    onOpen(ws);
    ws.on('close', () => onClose(ws));

    // user valid until
    ws.on('message', async (message: string) => {
      _authorizeUser(ws, message);
    });
  });

  return server;
};

const _authorizeUser = (userWS: WebSocket, token: string) => {
  const [success, { userId }] = checkToken(token);
  if (success && userId != null) {
    let extendedUserWs = userWS as ConnectedUser;
    extendedUserWs.userId = userId;

    connectedUsers = connectedUsers.filter((u) => u.userId != userId);
    connectedUsers.push(extendedUserWs);

    console.log('User authorized: ', userId);
  } else {
    console.log('User could not authorize with token: ', token);
  }
  console.log(
    'Currently connected users: ',
    connectedUsers.map((u) => u.userId).join(', ')
  );
};

const _messageTemplateAsync = async (
  room: string | Room,
  messageType: MessageType,
  data?: any
) => {
  const roomSession = room as Room;
  const roomId = room as string;
  if (roomSession?.roomId != null) {
    return await _roomMessageTemplateAsync(roomSession, messageType, data);
  } else {
    return await _roomIdMessageTemplateAsync(roomId, messageType, data);
  }
};

const _roomIdMessageTemplateAsync = async (
  roomId: string,
  messageType: MessageType,
  data?: any
) => {
  const room = await getRoomSessionAsync(roomId);
  if (room == null) {
    throw `Cannot find room with given ID: ${roomId}`;
  }
  return await _roomMessageTemplateAsync(room, messageType, data);
};

const _roomMessageTemplateAsync = async (
  room: Room,
  messageType: MessageType,
  data?: any
) => {
  if (room == null) {
    throw `Cannot find room with given ID`;
  }

  const messages: BrodcastModel[] = room.users.map<BrodcastModel>((u) => ({
    userId: u.userId,
    message: {
      type: messageType,
      data
    }
  }));

  sendBroadcastedWsMessages(...messages);
};

export const onUserRoomReadyAsync = async (room: string | Room) =>
  await _messageTemplateAsync(room, 'room_ready');

export const onUserRoomJoinedAsync = async (room: string | Room) =>
  await _messageTemplateAsync(room, 'room_joined');

export const onUserRoomLeftAsync = async (room: string | Room) =>
  await _messageTemplateAsync(room, 'room_left');

export const onRoundStartedAsync = async (room: string | Room) =>
  await _messageTemplateAsync(room, 'round_started');

export const onRoundEndedAsync = async (room: string | Room) =>
  await _messageTemplateAsync(room, 'round_ended');

export const onGameStartedAsync = async (room: string | Room) =>
  await _messageTemplateAsync(room, 'game_started');

export const onGameEndedAsync = async (room: string | Room) =>
  await _messageTemplateAsync(room, 'game_ended');

export const onGameAbortedAsync = async (room: string | Room) => {
  await _messageTemplateAsync(room, 'game_aborted');
};

export const onTabooChangedAsync = async (room: string | Room) => {
  await _messageTemplateAsync(room, 'taboo_changed');
};
