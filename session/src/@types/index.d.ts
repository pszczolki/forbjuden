interface RequestSession {
  roomId?: string;
  userId: string;
}

interface JoinRoomResult {
  roomId: string;
  code: string;
  users: RoomPlayerResult[];
}

interface SessionDetailsResult {
  userId: string;
  roomId?: string;
}

interface SessionDetailsWithTokenResult extends SessionDetailsResult {
  token: string;
}

interface RoomLobbyResult {
  users: RoomPlayerResult[];
  code: string;
  roomId: string;
}

interface CommonDetailsResult {
  roomId: string;
  code: string;
  players: RoomPlayerResult[];
  game?: GameDetailsResult;
}

interface RoomPlayerResult {
  userId: string;
  readyToPlay: boolean;
}

interface GameDetailsResult {
  teamName: string;
  isYourTeamGuessing: boolean;
  areYouShowing: boolean;
  points: {
    yourTeam: number;
    enemyTeam: number;
  };
  gameEnded: boolean;
  round: number;
  roundStarted: boolean;
  roundEndDate?: Date;
  taboo?: TabooEntry;
}
