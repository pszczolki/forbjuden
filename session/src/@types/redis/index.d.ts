interface BrodcastModel {
  userId: string;
  message: SocketMessage;
}

interface Room {
  users: RoomPlayer[];
  roomId: string;
  code: string;
  settings: GameSettings;
  // game properties
  game?: TabooGame;
}

interface GameSettings {
  maxRounds: number;
  roundTime: number;
}

interface RoomPlayer {
  userId: string;
  readyToPlay?: boolean;
}

type TabooTeamEnum = 'teamA' | 'teamB';

interface TabooGame {
  gameEnded: boolean;
  //round
  roundStartDate?: Date;
  roundNumber: number;
  roundStarted: boolean;

  // teams
  guessingTeam: TabooTeamEnum;
  teamA: TabooTeam;
  teamB: TabooTeam;

  // taboo words
  usedTabooIds?: string[];
  currentTaboo?: TabooEntry;
}

interface TabooEntry {
  guessingWord: string;
  tabooWords: string[];
  tabooGuid: string;
}

interface TabooTeam {
  userIds: string[];
  points: number;
  showingUserId: string;
}
