interface TabooApiResponse {
  id: string;
  word: string;
  taboo: string[];
}

interface TabooApiRequest {
  ids: string[];
}
