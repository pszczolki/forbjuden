import Axios from 'axios';
import { makeGuid } from '../../utility';

const phraseApiOrigin = process.env.PHRASES_API_URL || 'http://localhost:5000';

export const getTabooWordsAsync = async (
  req: TabooApiRequest
): Promise<TabooApiResponse> => {
  // todo: for development purpose. remove later on
  if (process.env.NODE_ENV === 'development') {
    return new Promise((resolve) =>
      resolve({
        id: makeGuid(5),
        word: makeGuid(5),
        taboo: [makeGuid(4), makeGuid(5), makeGuid(6)]
      })
    );
  }

  // why PUT tho??
  const { status, data } = await Axios.put<TabooApiResponse>(
    `${phraseApiOrigin}/word`,
    req,
    { timeout: 10_000 }
  );

  if (status === 200) {
    return data;
  } else {
    throw 'Could not fetch Phrases from API';
  }
};
