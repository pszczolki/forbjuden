import session from 'express-session';
import connectRedis from 'connect-redis';
import {
  createToken,
  getStoreKey,
  makeGuid,
  onlyUniquePredicate
} from '../utility';
import {
  setValueAsync,
  getValueAsync,
  redisClient,
  deleteValueAsync,
  REDIS_HOST,
  REDIS_PORT
} from '../redis';
import _ from 'lodash';
import {
  onGameAbortedAsync,
  onGameEndedAsync,
  onGameStartedAsync,
  onRoundStartedAsync,
  onRoundEndedAsync,
  onTabooChangedAsync,
  onUserRoomJoinedAsync,
  onUserRoomLeftAsync,
  onUserRoomReadyAsync
} from '../websocket';
import moment from 'moment';
import { getTabooWordsAsync } from '../api/phrases';

const Teams: TabooTeamEnum[] = ['teamA', 'teamB'];

export const getRoomSessionAsync = async (roomId: string) => {
  const key = getStoreKey(roomId);
  const stringifiedRoom = (await getValueAsync(key)) as any;

  if (stringifiedRoom == null) {
    return undefined;
  }

  const room = JSON.parse(stringifiedRoom) as Room;
  if (room == null) {
    throw 'Given room does not exist';
  }
  return room;
};

export const setRoomSessionAsync = async (room: Room) => {
  await setValueAsync(getStoreKey(room.roomId), JSON.stringify(room));
};

const redisStore = connectRedis(session);

export const createSessionMiddleware = () => {
  return session({
    secret: 'ThisIsHowWeRoll',
    name: '_taboo-session',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false, httpOnly: false }, // Note that the cookie-parser module is no longer needed
    store: new redisStore({
      host: REDIS_HOST,
      port: REDIS_PORT,
      client: redisClient,
      ttl: 86400
    })
  });
};

const codeGuidLength = 5;
const roomGuidLength = 5;
const userGuidLength = 3;

export const userIdMiddleware = (req: any, resp: any, next: () => void) => {
  const session = req.session as RequestSession;

  if (!session.userId) {
    session.userId = makeGuid(userGuidLength);
    console.log('New user guid created:', session.userId);
  }

  next();
};

export const createRoom = async (
  session: RequestSession
): Promise<JoinRoomResult> => {
  const maxRounds = 5; //todo: move to request
  const roundTime = 60; // in seconds todo: move to request
  const roomId = makeGuid(codeGuidLength);
  const code = makeGuid(roomGuidLength);
  const userId = session.userId;

  const room: Room = {
    code,
    roomId,
    users: [{ userId: userId }],
    settings: { maxRounds, roundTime }
  };

  session.roomId = roomId;

  await setRoomSessionAsync(room);

  return { users: [{ userId, readyToPlay: false }], roomId, code };
};

export const joinRoom = async (
  session: RequestSession,
  roomId: string,
  code: string
): Promise<JoinRoomResult | null> => {
  const room = await getRoomSessionAsync(roomId);

  if (room?.code !== code) {
    throw 'Invalid room code';
  }

  if (room.game != null) {
    throw 'Game has already started. You cannot join now.';
  }

  session.roomId = room.roomId;
  room.users.push({ userId: session.userId });
  room.users = room.users.filter(
    onlyUniquePredicate<RoomPlayer>((u) => u.userId)
  );

  await setRoomSessionAsync(room);
  await onUserRoomJoinedAsync(room);

  return {
    roomId: room.roomId,
    code: room.code,
    users: room.users.map((u) => ({
      readyToPlay: u.readyToPlay || false,
      userId: u.userId
    }))
  };
};

export const leaveRoom = async (session: RequestSession): Promise<boolean> => {
  const roomId = session.roomId;
  if (roomId == null) {
    return false;
  }

  const room = await getRoomSessionAsync(roomId);

  if (room == null) {
    return false;
  }

  room.users = room.users.filter((u) => u.userId != session.userId);
  session.roomId = undefined;

  await onUserRoomLeftAsync(room);

  if (room.users.length === 0) {
    const roomSessionKey = getStoreKey(roomId);
    await deleteValueAsync(roomSessionKey);
  } else {
    setRoomSessionAsync(room!);
  }
  return true;
};

export const getDetails = async (
  session: RequestSession
): Promise<SessionDetailsWithTokenResult> => {
  const token = createToken(session.userId, session.roomId);

  return {
    userId: session.userId,
    roomId: session.roomId,
    token
  };
};

const _getTabooPhraseAsync = async (
  usedTabooIds: string[]
): Promise<TabooApiResponse> => {
  return await getTabooWordsAsync({ ids: usedTabooIds });
};

const _chooseGuessingTeam = (round: number) => Teams[round % Teams.length];

const _chooseShowingUserId = (currentUserId: string, userIds: string[]) =>
  userIds[(userIds.findIndex((u) => u == currentUserId) + 1) % userIds.length];

const MIN_PLAYERS = 4;
const _createGameIfPossibleAsync = async (room: Room) => {
  const { users } = room;

  if (users.length >= MIN_PLAYERS) {
    const shuffled = _.shuffle(users.map((u) => u.userId));
    const teamA = shuffled.splice(0, shuffled.length / 2);
    const teamB = shuffled;
    room.game = {
      gameEnded: false,
      // teams
      guessingTeam: _chooseGuessingTeam(1),
      teamA: { userIds: teamA, showingUserId: teamA[0], points: 0 },
      teamB: { userIds: teamB, showingUserId: teamB[0], points: 0 },
      // round
      roundStarted: false,
      roundNumber: 1
    };

    return true;
  }

  return false;
};

export const setLobbyReadyAsync = async (
  session: RequestSession,
  ready: boolean
): Promise<boolean> => {
  const { roomId, userId } = session;
  if (!roomId) {
    throw 'not in a room booy';
  }

  const room = await getRoomSessionAsync(roomId!);
  if (room == null) {
    throw `Room with id ${roomId} doesn't exist`;
  }

  if (room.game != null) {
    throw 'Game has already started';
  }

  const arePlayersReady = _setUserReady(room, userId, ready);

  const canStartGame =
    arePlayersReady && (await _createGameIfPossibleAsync(room));

  if (canStartGame) {
    await _startRoundAsync(room, false);
    await setRoomSessionAsync(room);
    await onGameStartedAsync(room);
  } else {
    await setRoomSessionAsync(room);
    await onUserRoomReadyAsync(room);
  }

  return true;
};

/**
 * @param room
 * @param userId
 * @returns canStartRound : boolean
 */
const _setUserReady = (room: Room, userId: string, ready: boolean): boolean => {
  const user = room.users.find((u) => u.userId == userId);
  if (user == null) {
    throw 'user is actually not in a room';
  }
  user.readyToPlay = ready;

  return room.users.every((u) => u.readyToPlay);
};

export const getLobby = async (
  session: RequestSession
): Promise<RoomLobbyResult> => {
  const roomId = session.roomId;

  if (!roomId) {
    throw 'not in a room booy';
  }

  const room = await getRoomSessionAsync(roomId!);

  return {
    users:
      room?.users.map((u) => ({
        userId: u.userId,
        readyToPlay: u.readyToPlay || false
      })) || [],
    code: room?.code || '',
    roomId: room?.roomId || ''
  };
};

export const setRoundReadyAsync = async (
  session: RequestSession,
  ready: boolean
): Promise<boolean> => {
  const { roomId, userId } = session;
  if (!roomId) {
    throw 'not in a room booy';
  }

  const room = await getRoomSessionAsync(roomId!);
  if (room == null) {
    throw `Room with id ${roomId} doesn't exist`;
  }

  if (room.game?.roundStarted) {
    throw 'Round has already started';
  }

  const user = room?.users.find((u) => u.userId == userId);
  if (user == null) {
    throw 'user is actually not in a room';
  }

  user.readyToPlay = ready;

  const canStartRound = room.users.every((u) => u.readyToPlay);

  if (canStartRound) {
    await _startRoundAsync(room, true);
  }

  await setRoomSessionAsync(room);
  await onUserRoomReadyAsync(room);
  return true;
};

export const scorePointOnRoundAsync = async (
  session: RequestSession,
  guessed: boolean,
  tabooGuid: string
) => {
  if (session.roomId == null) {
    throw 'User is not even in a room';
  }

  const room = await getRoomSessionAsync(session.roomId);
  if (!room) {
    throw "Room doesn't exist";
  }
  const { game } = room;

  if (!game) {
    throw 'Active game needed';
  }

  if (tabooGuid != game.currentTaboo?.tabooGuid) {
    throw 'Bro you need to sync. Your taboo guid is invalid';
  }

  _validateTime(room);
  _addPoints(game, guessed);
  await _setNewTabooPhraseAsync(game);

  await setRoomSessionAsync(room);
  await onTabooChangedAsync(room);
};

export const endRoundAsync = async (
  session: RequestSession,
  round: number
): Promise<void> => {
  if (session.roomId == null) {
    throw 'User is not even in a room';
  }

  const room = await getRoomSessionAsync(session.roomId);
  if (!room) {
    throw "Room doesn't exist";
  }

  const { game } = room;

  if (!game) {
    throw "Game didn't start";
  }

  _validateRound(room, round);
  const gameEnded = _prepareNextRound(room);

  await setRoomSessionAsync(room);
  if (gameEnded) {
    await onGameEndedAsync(room);
  } else {
    await onRoundEndedAsync(room);
  }
};

const _addPoints = (game: TabooGame, guessed: boolean) => {
  if (guessed) {
    const team = _getGuessingTeam(game);
    team.points++;
  }
};

/**
 * @param game
 * @param settings
 * @returns gameFinished : boolean
 */

const _prepareNextRound = ({ game, settings, users }: Room): boolean => {
  if (game == null) {
    throw 'Game did not start';
  }

  switch (game.guessingTeam) {
    case 'teamA':
      game.teamB.showingUserId = _chooseShowingUserId(
        game.teamB.showingUserId,
        game.teamB.userIds
      );
      break;
    case 'teamB':
      game.teamA.showingUserId = _chooseShowingUserId(
        game.teamA.showingUserId,
        game.teamA.userIds
      );
      break;
  }

  game.roundStartDate = undefined;
  game.roundStarted = false;
  // set users to not ready
  users.forEach((u) => (u.readyToPlay = false));

  if (game.roundNumber >= settings.maxRounds) {
    _finishGame(game);
    return true;
  } else {
    game.roundNumber++;
    game.guessingTeam = _chooseGuessingTeam(game.roundNumber);

    return false;
  }
};

const _getGuessingTeam = (game: TabooGame): TabooTeam => {
  if (game.guessingTeam === 'teamA') {
    return game.teamA;
  } else {
    return game.teamB;
  }
};

const _getUserTeam = (game: TabooGame, userId: string): TabooTeamEnum => {
  const { teamA, teamB } = game;
  if (teamA.userIds.includes(userId)) {
    return 'teamA';
  } else if (teamB.userIds.includes(userId)) {
    return 'teamB';
  } else {
    throw 'User is not a part of any team?!';
  }
};

const _validateTime = (room: Room) => {
  const now = moment.utc();
  const validUntil = moment(room.game?.roundStartDate).add(
    room.settings.roundTime,
    'seconds'
  );
  if (validUntil.isBefore(now)) {
    throw 'Time is up my dear';
  }
};

const _validateRound = (room: Room, round: number) => {
  const { game, settings } = room;
  if (game == null) {
    throw 'Game is not initialized';
  }

  if (game.roundNumber != round) throw 'Invalid round number';
  if (game.roundStarted != true) throw 'Round has not started';
};

const _finishGame = (game: TabooGame) => {
  if (!game) {
    throw "Cannot finish game that hasn't started!";
  }

  console.log('Game ended');
  const teamAPoints = game.teamA.points;
  const teamBPoints = game.teamB.points;

  if (teamAPoints > teamBPoints) {
    console.log('Team A won!');
  } else if (teamBPoints > teamAPoints) {
    console.log('Team B won!');
  } else {
    console.log("It's a TIE!");
  }

  game.gameEnded = true;
};

const _startRoundAsync = async (room: Room, notifyOnStart: boolean) => {
  if (!room.game) {
    throw "Game didn't start";
  }

  const { game } = room;

  if (game.roundStarted) {
    throw 'Round already started';
  }

  await _setNewTabooPhraseAsync(game);

  game.roundStarted = true;
  game.roundStartDate = moment.utc().toDate();

  if (notifyOnStart) {
    await onRoundStartedAsync(room);
  }
};

const _setNewTabooPhraseAsync = async (game: TabooGame) => {
  const {
    word: guessingWord,
    taboo: tabooWords,
    id
  } = await _getTabooPhraseAsync(game.usedTabooIds || []);

  game.currentTaboo = {
    guessingWord,
    tabooWords,
    tabooGuid: id
  };

  if (game.usedTabooIds != null) {
    game.usedTabooIds.push(id);
  } else {
    game.usedTabooIds = [id];
  }
};

export const abortGameAsync = async (session: RequestSession) => {
  const roomId = session.roomId;
  if (roomId == null) {
    throw 'User must be in a room first';
  }

  const room = (await getRoomSessionAsync(roomId))!;

  if (room.game == null) {
    throw 'Game must be active in order to abort.';
  }
  // todo: remove room in user sessions
  await deleteValueAsync(getStoreKey(roomId));

  await onGameAbortedAsync(room);
};

export const getGameDetailsAsync = async (
  session: RequestSession
): Promise<CommonDetailsResult> => {
  const roomId = session.roomId;
  if (roomId == null) {
    throw 'User must be in a room first';
  }

  const room = (await getRoomSessionAsync(roomId))!;
  const { game, settings } = room;

  let gameDetails: GameDetailsResult | undefined;

  if (game != null) {
    const yourTeam = _getUserTeam(game, session.userId);
    const guessingTeam = game.guessingTeam;

    const isYourTeamGuessing = yourTeam === guessingTeam;
    const areYouShowing =
      isYourTeamGuessing &&
      _getGuessingTeam(game).showingUserId === session.userId;
    const showTaboo = !isYourTeamGuessing || areYouShowing;
    const yourPoints =
      yourTeam == 'teamA' ? game.teamA.points : game.teamB.points;
    const enemyPoints =
      yourTeam == 'teamA' ? game.teamB.points : game.teamA.points;

    const roundEndDate = moment(game.roundStartDate)
      .add(settings.roundTime, 'seconds')
      .toDate();

    const teamName = yourTeam == "teamA" ? "Team A" : "Team B";
    
    gameDetails = {
      teamName: teamName,
      areYouShowing: areYouShowing,
      isYourTeamGuessing: isYourTeamGuessing,
      gameEnded: game.gameEnded,
      roundStarted: game.roundStarted,
      roundEndDate: roundEndDate,
      taboo: showTaboo ? game.currentTaboo : undefined,
      points: {
        yourTeam: yourPoints,
        enemyTeam: enemyPoints
      },
      round: game.roundNumber
    };
  }

  return {
    code: room.code,
    roomId: room.roomId,
    players: room.users.map((u) => ({
      userId: u.userId,
      readyToPlay: u.readyToPlay || false
    })),
    game: gameDetails
  };
};
