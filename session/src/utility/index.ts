import jwt from 'jsonwebtoken';

export const onlyUnique = (value: any, index: number, self: any[]) => {
  return self.indexOf(value) === index;
};

export const onlyUniquePredicate = <T>(predicate: (val: T) => any) => (
  value: any,
  index: number,
  self: any[]
) => {
  const thisValue = predicate(value);
  return self.findIndex((v) => predicate(v) == thisValue) === index;
};

export function makeGuid(length: number) {
  var result = '';
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const detailsSecret = 'ziolowy-eliksir';

export const createToken = (userId: string, roomId?: string) => {
  const payload: SessionDetailsResult = {
    userId: userId,
    roomId: roomId
  };
  const token = jwt.sign(payload, detailsSecret);
  return token;
};

export const checkToken = (token: string): [boolean, SessionDetailsResult] => {
  const sessDetails = jwt.verify(token, detailsSecret) as SessionDetailsResult;
  const success = sessDetails != null;
  return [success, sessDetails];
};

export const getStoreKey = (guid: string) => `store_${guid}`;

export const normalizePort = (port: any) => parseInt(port, 10);
