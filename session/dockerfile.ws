FROM node:12.20.0-alpine3.12 AS INSTALL
WORKDIR /app/install

COPY webpack.WS.prod.js .
COPY package.json .
COPY tsconfig.json .
COPY src ./src

RUN yarn install
RUN yarn run prod:ws

FROM node:12.20.0-alpine3.12 AS PROD

ENV REDIS_PORT=6379
ENV REDIS_HOST=localhost
ENV WS_PORT=7070

WORKDIR /app
COPY --from=INSTALL /app/install/prod/ws/index.js . 

ENTRYPOINT [ "node", "index.js" ]