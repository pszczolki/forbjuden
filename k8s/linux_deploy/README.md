# How to deploy on linux server using minikube

1. Run k8s deployments on minikube as described in k8s folders README.md
2. run redirect-minikube.sh.sh, it will expose minikube deployments to 0.0.0.0:3000 by default
