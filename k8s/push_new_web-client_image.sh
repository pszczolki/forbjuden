#!/bin/bash

if [ $# -ne 1 ]; then
    echo 1>&2 "$0: please specify new image tag. Example: bash $0 1.0"
    exit 2
fi

pushd ../client

echo Building image..
docker build -t temp_bash_forbjuden-web-client:$1 .

echo Tagging images...
docker tag temp_bash_forbjuden-web-client:$1 registry.gitlab.com/pszczolki/forbjuden/web-client:$1
docker tag temp_bash_forbjuden-web-client:$1 registry.gitlab.com/pszczolki/forbjuden/web-client:latest

echo Pushing new image registry.gitlab.com/pszczolki/forbjuden/web-client:$1
docker push registry.gitlab.com/pszczolki/forbjuden/web-client:$1

echo Pushing new image registry.gitlab.com/pszczolki/forbjuden/web-client:latest
docker push registry.gitlab.com/pszczolki/forbjuden/web-client:latest

echo Clearing temporary images...
docker rmi -f temp_bash_forbjuden-web-client:$1

popd
exit 0