#!/bin/bash

if [ $# -ne 1 ]; then
    echo 1>&2 "$0: please specify new tag for all images. Example: bash $0 1.0"
    exit 2
fi

bash push_new_phrases-api_image.sh $1
bash push_new_session-api_image.sh $1
bash push_new_websocket_image.sh $1
bash push_new_web-client_image.sh $1
exit 0