# INFRASTRUCTURE

- configmap.k8s.yml - stores project configuration values.
- gitlab-registry-secret.yml - secret with gitlab container registry authentication.
- namespace.k8s.yml - namespace for project
- phrases-api.k8s.yml - phrases api deployment
- phrases-api-service.k8s.yml - service for phrases api deployment
- session-api.k8s.yml - session api deploymeny
- session-api-service.k8s.yml - service for session api deployment
- session-api-ingress.k8s.yml - ingress configuration for session api
- web-client.k8s.yml - web client deploymeny
- web-client-service.k8s.yml - service for web client deployment
- web-client-ingress.k8s.yml - ingress configuration for web client
- websocket.k8s.yml - websocket deploymenty
- websocket-service.k8s.yml - service for websocket deployment
- websocket-ingress.k8s.yml - ingress configuration for websocket
- redis.k8s.yml - redis deployment (used for session api)
- mongo.k8s.yml - mongo db deploymeny (used for phrases api)

# HOW TO K8S

1. Install kubernetes cli (1.20.0) and minikube (1.16.0) (min 4cpus and 2048 mb memory). 
- for windows (hyperv is required) `minikube start --cpus 4 --memory 2048 --driver=hyperv`)
- for linux distros (docker is required) `minikube start --cpus 4 --memory 2048 --driver=docker`)

2. Enabled ingress - run `minikube addons enable ingress`

3. Apply k8s config files: `kubectl apply -f ./` (make sure to choose k8s folder)

4. Make sure all pods, services, etc. are running using `kubectl get all -n forbjuden`. In case of problems only `kube describe ...` and google can save you.

5. Run `kubectl get ingress -n forbjuden --watch` wait for assigned ip ADDRESSes and add ADDRESS ips with HOSTS to end of your `/etc/hosts` file. Example

```txt
# Kubernetes taboo
172.19.163.225 app.taboo.io
172.19.163.225 api.taboo.io
172.19.163.225 ws.taboo.io
# End of section
```

6. Run app on http://app.taboo.io


# SCRIPTS
Scripts use gitlab container registry by default.
1. push_new_web-client_image.sh 
2. push_new_session-apiimage.sh
3. push_new_websocket_image.sh
4. push_new_phrases-api_image.sh
5. push_new_all.sh
6. linux_deploy/redirect-minikube.sh

Example use of scripts 1-5: `bash <script> <image-tag>`. It will create and push new images for corresponding services with given tags: `<image-tag>` and `latest`.

Script 6 is used for ssh tunneling from minikube to localhost.