import secrets

from flask import Flask, Response, request

from db import Word
from db import init_values
from db import initialize_db
import os

app = Flask(__name__)
mongo_host = os.getenv("MONGO_HOST") or 'localhost'
mongo_port = os.getenv("MONGO_PORT") or '27017'

app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://' + mongo_host + ':' + mongo_port
}

initialize_db(app)
init_values()


@app.route('/healthz')
def healthz():
    return {'status': 'OK'}, 200


@app.route('/words')
def get_words():
    words = Word.objects().to_json()
    return Response(words, mimetype="application/json", status=200)


@app.route('/word', methods=['POST'])
def add_word():
    body = request.get_json()
    for word in body:
        Word(**word).save()
    return '', 200


@app.route('/word/<id>', methods=['PUT'])
def update_words(id):
    body = request.get_json()
    Word.objects.get(id=id).update(**body)
    return '', 200


@app.route('/word/<id>', methods=['DELETE'])
def delete_word(id):
    Word.objects.get(id=id).delete()
    return '', 200


@app.route('/word', methods=['PUT'])
def get_word():
    req = request.get_json()
    ids = req['ids']
    words = Word.objects()
    words_range = int(len(words) * 0.75)
    if len(ids) < len(words):
        word = get_random_word(ids, words)
    else:
        word = get_random_word(ids[len(ids) - words_range:], words)
    return {'id': str(word.id), 'word': str(word.word), 'taboo': list(word.taboo)}, 200


def get_random_word(ids, words):
    word = secrets.choice(words)
    word_id = (str(word.id))
    if word_id in ids:
        return get_random_word(ids, words)
    return word


if __name__ == '__main__':
    app.run(host='0.0.0.0')
