## About 

Phrases management API. The service works with a MongoDB where phrases are stored.

# How to run
For setting up whole environment kubernetes setup is preferred. See `/k8s/README.md`

## How to run locally
- Ensure MongoDB is running, also can be run with docker by `run_mongo.sh`
- Run `pip install -r requirements.txt`
- Run `python app.py`

## Docker commands

### Image builds

`docker build -t forbjuden-phrases-api:<version> -f dockerfile.api .`


### Container run

`docker run -p <port>:5000 -e MONGO_PORT=<mongo-port> -e MONGO_HOST=<mongo-host> forbjuden-phrases-api:<version>`

- `-e MONGO_HOST=host.docker.internal` for local mongo


