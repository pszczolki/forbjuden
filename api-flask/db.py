from flask_mongoengine import MongoEngine
import json

db = MongoEngine()


class Word(db.Document):
    word = db.StringField(required=True, unique=True)
    taboo = db.ListField(db.StringField(), required=True)


def initialize_db(app):
    db.init_app(app)


def init_values():
    if not Word.objects:
        json_file = open('data.json')
        data = json.load(json_file)
        for record in data:
            Word(**record).save()
